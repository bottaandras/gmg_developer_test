/**
 * Created by Bandi on 2016.01.09..
 */
$(document).ready(function() {

    $.getJSON('income_publisher.php', function(data) {
        console.log(data);
        if (data.error){
            setInformationText(data.error);
            return;
        }

        setInformationText(data.information);
        drawCSVButton();
        drawTableData(data.incomeArray);
    });

});

function setInformationText(info)
{
    $("#income-info").text(info);
}

function drawTableData(tableData)
{
    var tbl_body = "<tr><th>Day</th><th>Income</th></tr>";
    var odd_even = false;
    $.each(tableData, function(index, value) {
        var tbl_row = "<td>"+value.day+"</td><td>"+value.amount+"</td>";
        tbl_body += "<tr class=\""+( odd_even ? "odd" : "even")+"\">"+tbl_row+"</tr>";
        odd_even = !odd_even;
    });
    $("#income-table").html(tbl_body);
}

function drawCSVButton()
{
    var button = "<button id=\"income-csv-button\">Download in CSV</button>";
    $(document).on("click", "#income-csv-button", function() {
        window.open("income_publisher.php?format=csv", "_blank");
    });
    $("#csv-button-div").html(button);
}
