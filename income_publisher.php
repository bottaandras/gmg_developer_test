<?php
/**
 * Created by PhpStorm.
 * User: Bandi
 * Date: 2016.01.07.
 * Time: 19:55
 */

$emailAddr = 'modulebugbear@randomthings.com';
$connectionParams = [
    'db_host' => '127.0.0.1',
    'db_port' => '3306',
    'db_name' => 'developer_test',
    'db_charset' => 'utf8',
    'db_user' => 'apptest',
    'db_pass' => 'apptest',
];

class DBConnection
{
    private $dbParams;

    public function __construct($dbParams)
    {
        $this->dbParams = $dbParams;
    }

    public function createDBConnection()
    {
        try {
            $connection = new PDO('mysql:host='.$this->dbParams['db_host'].';port='.$this->dbParams['db_port'].';dbname='.$this->dbParams['db_name'].';charset='.$this->dbParams['db_charset'].'',
                $this->dbParams['db_user'], $this->dbParams['db_pass']);
        } catch (PDOException $exc) {
            return "Error connect to database";
        }

        return $connection;
    }
}

class User
{
    public $email;

    public function __construct($email)
    {
        $this->email = $email;
    }
}

class IncomeDataToSend
{
    public $incomeArray;
    public $information;

    public function __construct($email, $incomeArray)
    {
        $this->incomeArray = $incomeArray;
        $this->information = "Daily income from banners of user: ".$email;
    }
}

class UserBannerIncomeTracker
{
    private $dbConnection;
    private $user;

    public function __construct(PDO $dbConnection, User $user)
    {
        $this->user = $user;
        $this->dbConnection = $dbConnection;
    }

    private function getUserAllDailyIncome()
    {
        $queryParams = [':email' => $this->user->email];
        $resultsArray = [];

        $stmt = $this->dbConnection->prepare(
            'select stat.day as day, SUM(amount) as amount
        from statistic stat
        where stat.banner_id IN (
            select bnr.id
            from banner bnr
            where bnr.user_id = (
                select usr.id
                from user usr
                where usr.email=:email
            )
        )
        GROUP BY stat.day
        ORDER BY stat.day
        DESC;'
        );
        $stmt->execute($queryParams);

        while ($row = $stmt->fetch()) {
            $resultsArray[] = ['day' => $row['day'], 'amount' => $row['amount']];
        }

        return $resultsArray;
    }

    private function generateCSVContentFromUserResults($resultsArray=[])
    {
        $content = "Daily income from banners of user: ".$this->user->email."\r\n";
        $content .= "Day;Income\r\n";
        foreach ($resultsArray as $row) {
            $content .= $row['day'].";".$row['amount']."\r\n";
        }

        return $content;
    }

    private function generateJSONContentFromUserResults($resultsArray=[])
    {
        $userIncomeData = new IncomeDataToSend($this->user->email, $resultsArray);
        $content = json_encode($userIncomeData);

        return $content;
    }

    private function printCSVContent($content)
    {
        header("Content-Type: text/csv");
        header('Content-disposition: filename="user_income.csv"');
        echo $content;
    }

    private function printJSONContent($content)
    {
        header("Content-Type: application/json");
        echo $content;
    }

    public function trackIncome()
    {
        $results = $this->getUserAllDailyIncome();
        if (isset($_GET['format']) && $_GET['format']=='csv') {
            $content = $this->generateCSVContentFromUserResults($results);
            $this->printCSVContent($content);
        } else {
            $content = $this->generateJSONContentFromUserResults($results);
            $this->printJSONContent($content);
        }
    }

}

function run($connectionParams, $emailAddr)
{
    $dbConnection = new DBConnection($connectionParams);
    if (!is_a($connection = $dbConnection->createDBConnection(), "PDO")) {
        header("Content-Type: application/json");
        echo json_encode(array('error'=>$connection), JSON_FORCE_OBJECT);
        die();
    }

    $user = new User($emailAddr);
    $userBannerTracker = new UserBannerIncomeTracker($connection, $user);
    $userBannerTracker->trackIncome();
}

run($connectionParams, $emailAddr);
